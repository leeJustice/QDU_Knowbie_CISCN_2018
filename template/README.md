# 不需要验证码破解环节

# 修改Flag命令：echo CISCN{this_is_a_new_flag} > ./flag.txt

# flag.txt位于 www/backend/ 下

# Hint

1. 积分攒到50,000会有惊喜
2. 漏洞存在于解锁的新功能中
3. 不存在弱口令及注入问题

# 测试命令

python checker.py 127.0.0.1 80 csrfmiddlewaretoken

# 漏洞利用脚本

exp.py为漏洞利用脚本


# 启动方法

cd ./QDU_Knowbie_CISCN_2018/template/deploy

docker-compose up -d